Title: gnome-desktop/gnome-tweak-tool has been renamed to gnome-desktop/gnome-tweaks
Author: Rasmus Thomsen <cogitri@exherbo.org>
Content-Type: text/plain
Posted: 3rd of March 2018
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: gnome-desktop/gnome-tweak-tools

Upstream renamed gnome-tweak-tools to gnome-tweaks since version 3.28.0.
Please uninstall gnome-tweak-tool and install gnome-tweaks afterwards to
upgrade from 3.26 to 3.28.

1. Remove gnome-tweak-tool:

    cave uninstall gnome-tweak-tool -x

2. Install gnome-tweaks

    cave resolve gnome-tweaks -x

