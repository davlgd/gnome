# Copyright 2011 Brett Witherspoon <spoonb@exherbo.org>
# Copyright 2013 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require gsettings
require meson

SUMMARY="Extensions to modify and extend GNOME Shell functionality and behavior"
HOMEPAGE="https://wiki.gnome.org/Projects/GnomeShell/Extensions"

LICENCES="GPL-2"

SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="classic-mode [[ description = [ enable plugins required for GNOME Classic Mode ] ]]
    (
        linguas:
            af an ar as be bg bn_IN bs ca ca@valencia cs da de el en_GB eo es et eu fa fi fr fur gd
            gl gu he hi hr hu id is it ja kk km kn ko lt lv mjw ml mr ms nb ne nl oc or pa pl pt
            pt_BR ro ru sk sl sr sr@latin sv ta te tg th tr uk vi zh_CN zh_HK zh_TW
    )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.6]
        virtual/pkg-config[>=0.22]
        classic-mode? ( dev-lang/sassc )
    build+run:
        dev-libs/glib:2[>=2.26] [[ note = [ For GSettings ] ]]
    run:
        gnome-desktop/gnome-shell[=$(ever range 1)*]
        gnome-desktop/gnome-menus:3.0[gobject-introspection]
    suggestion:
        gnome-desktop/gnome-tweaks [[ description = [ Enable/disable extensions ] ]]
"

MESON_SRC_CONFIGURE_PARAMS=( -Dextension_set=all )
MESON_SRC_CONFIGURE_OPTION_SWITCHES=( 'classic-mode classic_mode' )

