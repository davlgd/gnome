# Copyright 2022 Marc-Antoine Perennou <keruspe@exherbo.org>
# Copyright 2022 Calvin Walton <calvin.walton@kepstin.ca>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require freedesktop-desktop
require gsettings
require gtk-icon-cache
require meson

SUMMARY="A simple Text Editor for GNOME"
DESCRIPTION="Text Editor is a simple editor for GNOME focused on being a good general purpose default editor."

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-spell/enchant:2[>=2.2.0]
        app-text/editorconfig
        dev-libs/glib:2[>=2.73]
        dev-libs/icu:= [[ note = [ src/enchant/meson.build ] ]]
        dev-libs/libadwaita:1[>=1.2_alpha]
        gnome-desktop/gtksourceview:5[>=5.5.0]
        x11-libs/cairo [[ note = [ implicit ] ]]
        x11-libs/gtk:4.0[>=4.7]
        x11-libs/pango [[ note = [ implicit ] ]]
    test:
        dev-libs/appstream-glib
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Denchant=enabled
    -Dbugreport_url=https://gitlab.exherbo.org/exherbo/gnome/-/issues
)

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

