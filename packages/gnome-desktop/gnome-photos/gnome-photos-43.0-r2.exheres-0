# Copyright 2013 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings gtk-icon-cache
require meson

SUMMARY="Access, organize and share your photos on GNOME"
HOMEPAGE="https://wiki.gnome.org/Apps/Photos"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-libs/libxml2:2.0
        dev-util/desktop-file-utils
        gnome-desktop/yelp-tools
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config
    build+run:
        app-pim/tracker:3.0
        app-pim/tracker-miners:3.0
        dev-libs/at-spi2-core[>=2.52.0]
        dev-libs/libhandy:1[>=1.1.90]
        dev-libs/libdazzle:1.0[>=3.26.0]
        dev-libs/gexiv2[>=0.14.0]
        dev-libs/glib:2[>=2.62.0][gobject-introspection]
        gnome-desktop/gobject-introspection:1
        gnome-desktop/gsettings-desktop-schemas
        gnome-desktop/geocode-glib:2.0
        gnome-desktop/gnome-desktop:4[legacy]
        gnome-desktop/gnome-online-accounts[>=3.8.0]
        media-libs/babl
        media-libs/gegl:0.4[>=0.4.0][jpeg2000][raw]
        media-libs/libpng:1.6
        sys-libs/libportal[providers:gtk3]
        x11-libs/cairo[>=1.14.0]
        x11-libs/gdk-pixbuf:2.0[>=2.36.8]
        x11-libs/gtk+:3[>=3.22.16]
        x11-libs/pango
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=8] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
    suggestion:
        gnome-desktop/gnome-online-miners [[ description = [ For crawling facebook photos ] ]]
"
# gnome-desktop/libgdata[>=0.17.13][online-accounts]

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-build-accept-both-babl-and-babl-0.1.patch
)

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddogtail=false
    -Dflatpak=false
    -Dinstalled_tests=false
    -Dmanuals=false
)

pkg_postinst() {
    gtk-icon-cache_pkg_postinst
    gsettings_pkg_postinst
}

pkg_postrm() {
    gtk-icon-cache_pkg_postrm
    gsettings_pkg_postrm
}

