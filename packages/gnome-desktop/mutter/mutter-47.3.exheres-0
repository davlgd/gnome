# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2011 Brett Witherspoon <spoonb@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings meson

SUMMARY="Clutter and metacity based compositing window manager"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    doc
    eglstream [[ description = [ eglstream support (proprietary Nvidia driver) ] ]]
    gobject-introspection
    remote-desktop [[ description = [ Support remote desktop and screen casting ] ]]
    wacom [[ description = [ Use libwacom for tablet identification ] ]]

    ( providers: elogind systemd ) [[
        *description = [ Session tracking provider ]
        number-selected = at-most-one
    ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.6]
        sys-kernel/linux-headers[>=6.0]
        sys-libs/wayland-protocols[>=1.36]
        virtual/pkg-config[>=0.21]
        x11-libs/libxcvt [[ note = [ for the cvt command line tool ] ]]
        doc? ( dev-doc/gi-docgen[>=2021.1] )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.5] )
    build+run:
        dev-libs/at-spi2-core[>=2.52.0]
        dev-libs/fribidi[>=1.0.0]
        dev-libs/glib:2[>=2.81.1][gobject-introspection(+)?]
        dev-libs/libdisplay-info
        dev-libs/libei[>=1.0.901]
        dev-libs/libglvnd
        gnome-desktop/gnome-desktop:4
        gnome-desktop/gnome-settings-daemon:3.0
        gnome-desktop/gsettings-desktop-schemas[>=47.0][gobject-introspection?]
        gnome-desktop/libgudev[>=238]
        gnome-desktop/zenity
        media-libs/lcms2[>=2.6]
        sys-apps/colord[>=1.4.5]
        media-libs/fontconfig
        media-libs/libcanberra[>=0.26]
        sys-apps/dbus
        sys-libs/libinput[>=1.26.0]
        sys-libs/wayland[>=1.23.0]
        x11-apps/xkeyboard-config
        x11-dri/libdrm[>=2.4.83]
        x11-dri/mesa[>=21.3]
        x11-libs/cairo[>=1.10.0]
        x11-libs/graphene:1.0[>=1.10.2][gobject-introspection?]
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk:4.0[>=4.0.0][X][?media][wayland]
        x11-libs/harfbuzz[>=2.6]
        x11-dri/libdrm[>=2.4.118]
        x11-libs/libICE
        x11-libs/libxkbcommon[>=0.4.3][X]
        x11-libs/pango[>=1.46.0]
        x11-libs/pixman:1[>=0.42]
        x11-libs/startup-notification[>=0.7]
        (
            x11-libs/libSM
            x11-libs/libX11[>=1.7.0]
            x11-libs/libXau
            x11-libs/libXcomposite[>=0.2]
            x11-libs/libXcursor
            x11-libs/libXdamage
            x11-libs/libXext
            x11-libs/libXfixes[>=6]
            x11-libs/libXi[>=1.6.99.1]
            x11-libs/libXinerama
            x11-libs/libXrandr[>=1.5.0]
            x11-libs/libXrender
            x11-libs/libXtst
            x11-libs/libxcb
            x11-libs/libxkbfile
        ) [[ note = [ No option provided yet to take influence on have_x11 ] ]]
        eglstream? ( sys-libs/egl-wayland )
        providers:elogind? (
            sys-apps/eudev
            sys-auth/elogind
        )
        providers:systemd? ( sys-apps/systemd )
        remote-desktop? ( media/pipewire[>=1.2.0] )
        wacom? ( x11-libs/libwacom[>=0.19] )
    test:
        dev-python/python-dbusmock[python_abis:*(-)?]
        x11-libs/gtk+:3[>=3.19.8][gobject-introspection?][wayland]
        x11-utils/xvfb-run
    run:
        sys-apps/upower[>=0.99.0]
        x11-server/xwayland
    recommendation:
        sys-apps/iio-sensor-proxy [[ description = [ orientation handling ] ]]
        gnome-desktop/gnome-themes-extra [[
            description = [ Provides default GNOME 3 theme (Adwaita) ]
        ]]
"

# Bundled clutter fails its tests
RESTRICT="test"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dopengl=true
    -Dgles2=true
    -Dlibdisplay_info=enabled
    -Degl=true
    -Dglx=true
    -Dwayland=true
    -Dnative_backend=true
    -Dudev=true
    -Dsound_player=true
    -Dstartup_notification=true
    -Dsm=true
    -Dlibgnome_desktop=true
    -Dcogl_tests=false
    -Dkvm_tests=false
    -Dtty_tests=false
    -Dprofiler=false
    -Dverbose=true
    -Dx11=true
    -Dxwayland_initfd=enabled
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'doc docs'
    'eglstream egl_device'
    'eglstream wayland_eglstream'
    'gobject-introspection introspection'
    'remote-desktop remote_desktop'
    'providers:systemd systemd'
    'wacom libwacom'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dclutter_tests=true -Dclutter_tests=false'
    '-Dinstalled_tests=true -Dinstalled_tests=false'
    '-Dmutter_tests=true -Dmutter_tests=false'
)

