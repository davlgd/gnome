# Copyright 2008, 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require gnome.org [ suffix=tar.xz ] gsettings gtk-icon-cache freedesktop-desktop
require meson vala

export_exlib_phases pkg_postrm pkg_postinst

SUMMARY="Keyring and PGP manager"
HOMEPAGE="https://wiki.gnome.org/Apps/Seahorse"

LICENCES="
    GPL-2
    CCPL-Attribution-ShareAlike-3.0 [[ note = [ Help files ] ]]
"
SLOT="0"
MYOPTIONS="
    avahi
    ldap [[ description = [ Publish and retrieve keys via LDAP ] ]]
"

DEPENDENCIES="
    build:
        dev-libs/libxslt
        gnome-desktop/yelp-tools
        virtual/pkg-config[>=0.20]
    build+run:
        app-crypt/gpgme[>=1.14.0]
        app-crypt/gnupg[>=2.2.0]
        dev-libs/glib:2[>=2.66]
        dev-libs/libhandy:1[>=1.6.0][vapi]
        dev-libs/libpwquality
        dev-libs/libsecret:1[>=0.16][vapi]
        gnome-desktop/gcr:0[>=3.38][vapi]
        gnome-desktop/gnome-keyring:1[>=3.1.5]
        gnome-desktop/libsoup:3.0
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3[>=3.24.0]
        avahi? ( net-dns/avahi[>=0.6] )
        ldap? ( net-directory/openldap[>=2.4.21] )
"

# Fails when flatpak is available
RESTRICT=test

MESON_SRC_CONFIGURE_PARAMS=(
    -Dhelp=true
    -Dpgp-support=true
    -Dcheck-compatible-gpg=false
    -Dpkcs11-support=true
    -Dkeyservers-support=true
    -Dhkp-support=true
    -Dmanpage=true
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'avahi key-sharing'
    'ldap ldap-support'
)

seahorse_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

seahorse_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

