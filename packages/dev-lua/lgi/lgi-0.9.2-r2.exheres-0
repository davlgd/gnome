# Copyright 2013 Jakob Nixdorf <flocke@shadowice.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=pavouk tag=${PV} ] lua [ multiunpack=true whitelist="5.1 5.2 5.3" ]
require test-dbus-daemon

SUMMARY="Lua bindings for gnome/gobject using gobject-introspection library"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[gobject-introspection]
        dev-libs/libffi:=
        gnome-desktop/gobject-introspection:1
    test:
        x11-libs/cairo
        x11-libs/pango
"

prepare_one_multibuild() {
    default

    # disable some tests (they want to access the Xorg server -> access violation in sydbox and
    # tests fail)
    edo sed -i \
        -e "/'gtk.lua',/d" \
        -e "/'gobject.lua',/d" \
        "${WORKBASE}"/${MULTIBUILD_CLASS}/${MULTIBUILD_TARGET}/${PNV}/tests/test.lua
}

compile_one_multibuild() {
    emake PKG_CONFIG=$(exhost --tool-prefix)pkg-config LUA_CFLAGS=-I/usr/$(exhost --target)/include/lua$(lua_get_abi)
}

test_one_multibuild() {
    test-dbus-daemon_start
    emake PKG_CONFIG=$(exhost --tool-prefix)pkg-config LUA=/usr/$(exhost --build)/bin/lua$(lua_get_abi) check
    test-dbus-daemon_stop
}

install_one_multibuild() {
    emake -j1 DESTDIR="${IMAGE}" PREFIX=/usr LUA_VERSION=$(lua_get_abi) LUA_LIBDIR=$(lua_get_libdir) install
    emagicdocs
}

