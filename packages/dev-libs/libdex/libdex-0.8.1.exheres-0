# Copyright 2023 Calvin Walton <calvin.walton@kepstin.ca>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require vala [ with_opt=true ]
require meson [ cross_prefix=true ]

SUMMARY="Future-based programming for GLib-based applications"

LICENCES="LGPL-2.1"
SLOT="1"
PLATFORMS="~amd64"
MYOPTIONS="
    gobject-introspection
    gtk-doc [[ requires = [ gobject-introspection ] ]]
    sysprof [[ description = [ Provide ancillary profiling information when run under Sysprof ] ]]
    vapi [[ requires = [ gobject-introspection ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        gtk-doc? ( dev-doc/gi-docgen )
    build+run:
        dev-libs/glib:2[>=2.68][gobject-introspection(+)?]
        sys-libs/liburing[>=0.7]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.68] )
        sysprof? ( gnome-desktop/sysprof )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dexamples=false
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc docs'
    vapi
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'gobject-introspection introspection'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

pkg_setup() {
    vala_pkg_setup
    meson_pkg_setup
}

