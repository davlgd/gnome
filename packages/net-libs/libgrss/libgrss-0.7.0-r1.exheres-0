# Copyright 2023 Calvin Walton <calvin.walton@kepstin.ca>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]

SUMMARY="Glib abstraction to handle feeds in RSS, Atom, and other formats"

LICENCES="LGPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gobject-introspection"

DEPENDENCIES="
    build:
        virtual/pkg-config
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.30.0] )
    build+run:
        dev-libs/glib:2[>=2.42.1][gobject-introspection(+)?]
        dev-libs/libxml2:2.0[>=2.9.2]
        gnome-desktop/libsoup:2.4[>=2.48.0]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    # Ships prebuilt gtk-doc html
    --disable-gtk-doc
    # Package incorrectly uses AC_PATH_PROG to find pkg-config, which needs an absolute path
    "PKG_CONFIG=/usr/host/bin/$(exhost --tool-prefix)pkg-config"
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
)

