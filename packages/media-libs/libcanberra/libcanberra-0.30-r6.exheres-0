# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require systemd-service
require option-renames [ renames=[ 'gtk providers:gtk2' 'gtk3 providers:gtk3' ] ]
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="XDG Sound Theme and Name Specification Implementation"
HOMEPAGE="http://0pointer.de/lennart/projects/${PN}"
DOWNLOADS="${HOMEPAGE}/${PNV}.tar.xz"

LICENCES="LGPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    alsa doc gstreamer pulseaudio
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( providers: gtk2 gtk3 )
"

DEPENDENCIES="
    build:
        dev-doc/gtk-doc[>=1.32-r1]
        virtual/pkg-config[>=0.20]
        doc? ( dev-doc/gtk-doc[>=1.9] )
    build+run:
        media-libs/libvorbis
        alsa? ( sys-sound/alsa-lib[>=1.0.0] )
        gstreamer? (
            media-libs/gstreamer:1.0[>=0.10.15]
            media-plugins/gst-plugins-base:1.0[gstreamer_plugins:vorbis]
        )
        providers:eudev? ( sys-apps/eudev )
        providers:gtk2? (
            dev-libs/glib:2[>=2.32]
            x11-libs/gtk+:2[>=2.20.0]
            x11-libs/libX11
        )
        providers:gtk3? (
            dev-libs/glib:2[>=2.32]
            x11-libs/gtk+:3[X]
            x11-libs/libX11
        )
        providers:systemd? ( sys-apps/systemd )
        pulseaudio? ( media-sound/pulseaudio[>=0.9.11] )
    recommendation:
        sound-themes/sound-theme-freedesktop
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-udev
    --disable-oss
    --disable-tdb
    --disable-lynx
    --with-systemdsystemunitdir=${SYSTEMDSYSTEMUNITDIR}
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    alsa
    'doc gtk-doc'
    'doc gtk-doc-html'
    'providers:gtk2 gtk'
    'providers:gtk3'
    gstreamer
    'pulseaudio pulse'
)

src_prepare() {
    # Need to update gtk-doc.make due to gtkdoc-mktmpl removal
    edo rm "${WORK}"/gtkdoc/gtk-doc.make
    edo gtkdocize --copy --docdir gtkdoc
    autotools_src_prepare
}

src_install() {
    default

    exeinto /etc/X11/xinit/xinitrc.d/
    doexe "${FILES}/50-libcanberra"

    # NOTE (compnerd) clean up /usr/bin if necessary
    # HAVE_ALSA && HAVE_UDEV -> /usr/bin/canberra-boot
    # HAVE_GTK_ANY -> /usr/bin/canberra-gtk-play
    if option !alsa && option !providers:gtk2 && option !providers:gtk3 ; then
        edo rmdir "${IMAGE}/usr/$(exhost --target)/bin"
    fi
}

