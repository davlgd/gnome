# Copyright 2014 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require meson
require vala [ with_opt=true vala_dep=true ]

SUMMARY="Library for handling media art"
HOMEPAGE="https://gitlab.gnome.org/GNOME/libmediaart"

LICENCES="GPL-2 LGPL-2.1"
SLOT="2.0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc

    vapi [[ requires = gobject-introspection ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.30.0] )
        gtk-doc? ( dev-doc/gtk-doc )
    build+run:
        dev-libs/glib:2[>=2.38.0][gobject-introspection(+)?]
        x11-libs/gdk-pixbuf:2.0[>=2.12.0]
"

MESON_SRC_CONFIGURE_PARAMS=(
    '-Dimage_library=gdk-pixbuf'
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
    vapi
)

# Tries to talk with udisks via dbus
RESTRICT="test"

