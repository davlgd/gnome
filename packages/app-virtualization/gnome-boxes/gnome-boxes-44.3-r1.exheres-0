# Copyright 2012-2013 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require gsettings
require meson
require vala [ vala_dep=true ]
require gtk-icon-cache freedesktop-desktop

SUMMARY="A simple GNOME 3 application to access remote or virtual systems"
HOMEPAGE="https://wiki.gnome.org/Boxes"

LICENCES="LGPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        gnome-desktop/yelp-tools
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        app-arch/libarchive[>=3.0.0]
        app-pim/tracker:3.0
        dev-libs/glib:2[>=2.44.0][gobject-introspection]
        dev-libs/libhandy:1[>=1.5.0][vapi]
        dev-libs/libosinfo:1.0[>=1.10.0][vapi][providers:soup3]
        dev-libs/libsecret:1[vapi]
        dev-libs/libusb:1[>=1.0.9]
        dev-libs/libxml2:2.0[>=2.7.8]
        gnome-desktop/gobject-introspection:1
        gnome-desktop/libgudev[>=165]
        gnome-desktop/libsoup:3.0
        net-libs/webkit:4.1
        sys-apps/systemd
        sys-apps/util-linux
        virtualization-lib/libvirt-glib:1.0[>=4.0.0][vapi]
        virtualization-ui/spice-gtk:3.0[>=0.32][vapi][usbredir][smartcard]
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3[>=3.22.2]
    run:
        app-virtualization/qemu[>=1.3][spice][smartcard]
        virtualization-lib/libvirt[kvm]
        x11-misc/shared-mime-info
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dflatpak=false
    -Duefi=true
)

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

